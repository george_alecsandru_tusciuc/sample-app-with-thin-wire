package thinwire.apps.helloworld;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import thinwire.ui.Application;
import thinwire.ui.Button;
import thinwire.ui.Frame;
import thinwire.ui.GridBox;
import thinwire.ui.Panel;
import thinwire.ui.TextField;
import thinwire.ui.event.ActionEvent;
import thinwire.ui.event.ActionListener;
import thinwire.ui.layout.SplitLayout;
import thinwire.ui.style.Color;

public class Main {
    private static String FIlE_NAME = "notes.txt";

    public static void main(String[] args) throws IOException {
        Frame container = Application.current().getFrame();
        container.setLayout(new SplitLayout(100, false));
        container.getStyle().getBackground().setColor(Color.SLATEGRAY);
        container.setTitle("Tjpad notes demo");

        Panel topPanel = new Panel();
        container.getChildren().add(topPanel);
        final GridBox gridBox = new GridBox();
        container.getChildren().add(gridBox);

        gridBox.setVisibleHeader(true);
        GridBox.Column col = new GridBox.Column();
        gridBox.getColumns().add(col);
        col.setName("Notes list");
        for (String line : readLines()) {
            gridBox.getRows().add(new GridBox.Row((Object[]) (new String[] { line })));
        }

        final TextField textField = new TextField("Write your notes here!");
        textField.setBounds(20, 20, 500, 30);
        topPanel.getChildren().add(textField);
        Button button = new Button("Save!");
        button.setBounds(600, 20, 60, 30);
        topPanel.getChildren().add(button);

        button.addActionListener("click", new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                gridBox.getRows().add(new GridBox.Row((Object[]) (new String[] { textField.getText() })));
                writeContent(textField.getText());
            }
        });
    }

    private static void writeContent(String content) {
        try {
            if (!readLines().isEmpty()) {
                content = System.lineSeparator() + content;
            }
            Files.write(FileSystems.getDefault().getPath(".", FIlE_NAME), content.getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private static List<String> readLines() {
        try {
            if (Files.exists(FileSystems.getDefault().getPath(".", FIlE_NAME))) {
                return Files.readAllLines(FileSystems.getDefault().getPath(".", FIlE_NAME), Charset.defaultCharset());
            } else {
                return new ArrayList<String>();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return null;
    }
}
